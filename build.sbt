name := "zkdissector"

version := "1.0"

scalaVersion := "2.9.1"

libraryDependencies += "org.apache.zookeeper" % "zookeeper" % "3.4.5" exclude("javax.jms", "jms") exclude("com.sun.jdmk", "jmxtools") exclude("com.sun.jmx", "jmxri")

libraryDependencies += "org.scala-lang" % "scala-swing" % "2.9.1"
