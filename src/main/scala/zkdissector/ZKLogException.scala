package zkdissector

class ZKLogException(msg: String) extends RuntimeException(msg)

object ZKLogException {
  
  def create(msg: String) : ZKLogException = new ZKLogException(msg)

  def create(msg: String, cause: Throwable) = new ZKLogException(msg).initCause(cause)

}