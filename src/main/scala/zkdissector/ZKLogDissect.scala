package zkdissector

import swing._
import java.io._
import java.util.zip._
import org.apache.jute._
import org.apache.zookeeper._
import org.apache.zookeeper.server.persistence._
import org.apache.zookeeper.txn._
import org.apache.zookeeper.server.util._
import java.util.Date
import org.apache.zookeeper.ZooDefs.OpCode
import java.text.SimpleDateFormat

object ZKLogDissector extends SimpleSwingApplication {
  setSystemLookAndFeel

  val home = System.getProperty("user.home");
  val names = Array("Date", "Client ID", "Type", "Path", "Data").asInstanceOf[Array[Any]]
  val dateFromat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

  def top = new MainFrame {
    menuBar = new MenuBar {
      contents += new Menu("File") {
        contents += new MenuItem(Action("Open") { println(title) })
        contents += new MenuItem(Action("Save as...") { println(title) })
      }
      contents += new Menu("View") {
        contents += new CheckMenuItem("Do not show session creation/deletion")
      }
    }
    title = "Scala Log Dissector"
    contents = ui
    maximize
  }

  lazy val ui = new BoxPanel(Orientation.Vertical) {

    val rowData: Array[Array[Any]] = load.toArray
    val table = new Table(rowData, names) {
      peer.getColumnModel().getColumn(0).setMinWidth(150)
      peer.getColumnModel().getColumn(0).setMaxWidth(150)
      peer.getColumnModel().getColumn(1).setMinWidth(130)
      peer.getColumnModel().getColumn(1).setMaxWidth(130)
      peer.getColumnModel().getColumn(2).setMinWidth(80)
      peer.getColumnModel().getColumn(2).setMaxWidth(80)
    }
    contents += new ScrollPane(table)
  }

  def load: List[Array[Any]] = {
    val fis = new FileInputStream("C:/fic/MORELLO-08393/annuaire/zk/log/version-2/log.6f00000001")
    val log = BinaryInputArchive.getArchive(fis)
    val fhdr = new FileHeader
    fhdr.deserialize(log, "fileheader")
    return readLogTransactions(log);
  }

  def readLogTransactions(log: BinaryInputArchive): List[Array[Any]] = {
    val crcValue = log.readLong("crcvalue")
    val bytes = log.readBuffer("txnEntry")

    if (bytes.length == 0) {
      // Done !
      return Nil;
    }

    val crc = new Adler32
    crc.update(bytes, 0, bytes.length)
    if (crcValue != crc.getValue())
      throw ZKLogException.create("CRC doesn't match " + crcValue +
        " vs " + crc.getValue())

    val hdr = new TxnHeader
    val txn = SerializeUtils.deserializeTxn(bytes, hdr)

    val date = dateFromat.format(new Date(hdr.getTime))
    if (log.readByte("EOR") != 'B') {
      throw new EOFException("Last transaction was partial.");
    }
    return Array(date, hdr.getClientId, op2String(hdr.getType)) ++ txn2String(txn) :: readLogTransactions(log)
  }

  def txn2String(txn: Record): Array[String] = {
    txn match {
      case null => Array("", "")
      case d: DeleteTxn => Array(d.getPath(), "")
      case c: CreateTxn => Array(if (c.getEphemeral) c.getPath + "(*)" else c.getPath, new String(c.getData()))
      case s: SetDataTxn => Array(s.getPath(), new String(s.getData()))
      case _ => Array("", txn.toString())
    }
  }

  def op2String(op: Int): String = {
    op match {
      case OpCode.notification => "notification"
      case OpCode.create => "create"
      case OpCode.delete => "delete"
      case OpCode.exists => "exists"
      case OpCode.getData => "getDate"
      case OpCode.setData => "setData"
      case OpCode.multi => "multi"
      case OpCode.getACL => "getACL"
      case OpCode.setACL => "setACL"
      case OpCode.getChildren => "getChildren";
      case OpCode.getChildren2 => "getChildren2";
      case OpCode.ping => "ping";
      case OpCode.createSession => "createSession";
      case OpCode.closeSession => "closeSession";
      case OpCode.error => "error";
      case _ => "unknown " + op
    }
  }

  def setSystemLookAndFeel() {
    import javax.swing.UIManager
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)
  }

}
