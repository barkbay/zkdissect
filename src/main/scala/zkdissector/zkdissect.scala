package zkdissector

import swing._
import java.io._
import java.util.zip._
import org.apache.jute._
import org.apache.zookeeper._
import org.apache.zookeeper.server.persistence._
import org.apache.zookeeper.txn._
import org.apache.zookeeper.server.util._
import java.util.Date
import org.apache.zookeeper.ZooDefs.OpCode

object Hi extends SimpleSwingApplication {
  setSystemLookAndFeel()

  val home = System.getProperty("user.home");
  val names = Array("Date", "Client ID", "Type", "Transaction").asInstanceOf[Array[Any]]

  def top = new MainFrame {
    title = "Scala Log Dissector"
    contents = ui
  }

  lazy val ui = new BoxPanel(Orientation.Vertical) {
    val rowData: Array[Array[Any]] = load.toArray
    val table = new Table(rowData, names)
    contents += new ScrollPane(table)
  }

  def load: List[Array[Any]] = {
    val fis = new FileInputStream(home + "/ZK/Logs/zkserver1/zklogs/version-2/log.900000001")
    val log = BinaryInputArchive.getArchive(fis)
    val fhdr = new FileHeader
    fhdr.deserialize(log, "fileheader")
    return readLogTransactions(log);
  }

  def readLogTransactions(log: BinaryInputArchive): List[Array[Any]] = {
    val crcValue = log.readLong("crcvalue")
    val bytes = log.readBuffer("txnEntry")

    if (bytes.length == 0) {
      // Done !
      return Nil;
    }

    val crc = new Adler32
    crc.update(bytes, 0, bytes.length)
    if (crcValue != crc.getValue())
      throw ZKLogException.create("CRC doesn't match " + crcValue +
        " vs " + crc.getValue())

    val hdr = new TxnHeader
    val txn = SerializeUtils.deserializeTxn(bytes, hdr)
    val date = new Date(hdr.getTime)
    if (log.readByte("EOR") != 'B') {
      throw new EOFException("Last transaction was partial.");
    }
    return Array(date, hdr.getClientId, op2String(hdr.getType), txn) :: readLogTransactions(log)
  }

  def op2String(op: Int): String = {
    op match {
      case OpCode.notification => "notification"
      case OpCode.create => "create"
      case OpCode.delete => "delete"
      case OpCode.exists => "exists"
      case OpCode.getData => "getDate"
      case OpCode.setData => "setData"
      case OpCode.multi => "multi"
      case OpCode.getACL => "getACL"
      case OpCode.setACL => "setACL"
      case OpCode.getChildren => "getChildren";
      case OpCode.getChildren2 => "getChildren2";
      case OpCode.ping => "ping";
      case OpCode.createSession => "createSession";
      case OpCode.closeSession => "closeSession";
      case OpCode.error => "error";
      case _ => "unknown " + op
    }
  }

  def setSystemLookAndFeel() {
    import javax.swing.UIManager
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)
  }

}
